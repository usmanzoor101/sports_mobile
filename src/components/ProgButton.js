import React, { Component } from 'react';
import AwesomeButton from "react-native-really-awesome-button";
import Color from "../common/Color";

const ProgButton = (props)=>{
    return (
        <AwesomeButton
            width={260}
            textColor='white'
            backgroundColor={Color.secondary}
            borderRadius={30}
            textSize={20}
            progress
            backgroundProgress={Color.primary}
            progressLoadingTime={2000}
            onPress={next => {
                setTimeout(()=>{
                    props.onClick();
                    next();
                }, 100);
                
                
            }}
        >
            LOGIN
    </AwesomeButton>


   
        ); 
    
}

export default ProgButton;
