import React from "react";
import {View , ActivityIndicator, StyleSheet} from 'react-native';

const MySpinner  = ({size}) =>{
    return(
        <View style={styles.spinnerstyle} >
        <ActivityIndicator size={size || "large"} />
        </View>

    );
};

const styles=StyleSheet.create({
    spinnerstyle:{
       // flex:1,
        justifyContent:'center',
        padding:20,
        marginTop:10
    }
});

export default MySpinner;