const MyData= [
    {
        title: 'Title 1',
        caption: 'Caption 1',
        url: 'http://placeimg.com/640/480/any',
    },
    {
        title: 'Title 2',
        caption: 'Caption 2',
        url: 'http://placeimg.com/640/480/house',
    }, {
        title: 'Title 3',
        caption: 'Caption 3',
        url: 'http://placeimg.com/640/480/any',
    }, {
        title: 'Title 4',
        caption: 'Caption 4',
        url: 'http://placeimg.com/620/440/car',
    }, {
        title: 'Title 5',
        caption: 'Caption 5',
        url: 'http://placeimg.com/620/420/animals',
    },
]

export default MyData;