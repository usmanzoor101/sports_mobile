import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ImageBackground,
    Image,
    TextInput,
    Dimensions,
    TouchableOpacity,
    StatusBar,
    
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Swiper from './Swiper';
import  Color from "../common/Color";
import axios from 'axios';



var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

export default class ItemDetails extends Component<{}> {


    render() {
        
        return (
            
            <View style={{ flex: 1,backgroundColor:Color.fourth}} >
                <Text style={styles.text1}>Product Details</Text>

              
                {/* <View>
                <Swiper />
                </View> */}

            </View>



        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        
    },
    imgBackground: {
        width: width,
        height: height,
    },
    text1: {
      
        fontSize: 26,
        fontWeight: 'bold',
        color: Color.secondary,
        fontStyle: 'italic'
    },
    text4: {
        alignSelf: 'center',
        fontSize: 17,
        color: Color.secondary,
        marginTop: 6,
    },
    inputText: {
        //backgroundColor:'transparent',
        backgroundColor: 'rgba(255,255, 255, 0.3)',
        width: 320,
        fontSize: 20,
        marginTop: 10,
        paddingLeft: 10,
        height: 50,
        color: 'white',
        borderWidth: 2,
        borderRadius: 5,
        borderColor: '#D0D0D0'
    },

    image: {
        width: 35,
        height: 35,
        marginTop: 25,
        marginRight: 10,
    },
    inputRow: {
        flexDirection: 'row',
        alignSelf: 'center',
    },
    loginButton: {
        borderColor: 'white', borderWidth: 1,
        width: width/2, borderRadius: 5,
        height:60,
        marginTop:50,
        alignSelf:'center',
        justifyContent:'center',
        backgroundColor: Color.secondary,
    },

    registerButton: {
        borderColor: 'white', borderWidth: 1,
        
        width: width, borderRadius: 5,
        backgroundColor: Color.secondary,
    },

    text: {
        color: 'white',
        fontSize: 20,
        alignSelf: 'center',
        fontWeight:'bold'
        
    },
    imageReg: {
        width: 23,
        height: 23,
        alignSelf: 'center',
    },

});