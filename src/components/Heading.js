import React, { Component } from 'react';
import {
    Text,
} from 'react-native';

import { Header, Left, Button, Icon, Right, Body, Title } from 'native-base';

export default class Heading extends Component {
    render() {
        return (
            <Header style={{backfaceVisibility: 'rgba(77, 175, 124, 1)',elevation:3,marginBottom:2}} >
                <Left>
                    <Button transparent
                        onPress={() => this.props.openDrawer()}
                    >
                        <Icon name='menu' />
                    </Button>
                </Left>
                <Body>
                    <Title>Sports Clothing</Title>
                </Body>
                <Right>
                    <Button transparent>
                        <Icon name='cart' />
                    </Button>
                </Right>
            </Header>
        );
    }
}

module.exports = Heading;