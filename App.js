/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import Routes from './Routes';
import HomePage from './src/pages/HomePage';
import MyOnboarding from './src/components/MyOnboarding';

    componentWillMount()
    {
      this.getData();
      
    }

render() {

  switch (this.state.onboarding) {
    case 'pending': return (<Onboarding />);
    case 'done': return (<Home />);
    default: return (<Loading />);
  };

}