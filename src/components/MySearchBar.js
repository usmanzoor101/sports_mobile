import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    ScrollView,
    Keyboard,
    Alert,
    TouchableOpacity,
    
} from "react-native";

import * as Animatable from 'react-native-animatable'

import { Icon, Thumbnail } from 'native-base';
export default class MySearchBar extends Component {

    state = {
        searchBarFocused: false
    }

    componentDidMount() {
        this.keyboardDidShow = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow)
        this.keyboardWillShow = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow)
        this.keyboardWillHide = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide)


    }

    keyboardDidShow = () => {
        this.setState({ searchBarFocused: true })
    }

    keyboardWillShow = () => {
        this.setState({ searchBarFocused: true })
    }

    keyboardWillHide = () => {
        this.setState({ searchBarFocused: false })
    }
    render() {
        return (
            <View >


                <Animatable.View animation='slideInRight' duration={2000} style={{ height: 50, backgroundColor: 'white', flexDirection: 'row', padding: 5, alignItems: 'center',marginHorizontal:10 }}>
                    <Animatable.View animation={this.state.searchBarFocused ? "fadeInLeft" : "fadeInRight"} duration={400}>
                        <Icon name={"search"} style={{ padding:10 }} />
                    </Animatable.View>
                    <TextInput placeholder="Search" style={{ fontSize: 18, marginLeft: 15, flex: 1,padding:10,borderRadius:50, backgroundColor:'rgba(242, 241, 239, 0.2)' }} />
                </Animatable.View>

            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});