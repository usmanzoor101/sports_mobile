import React, { Component } from 'react';
import { Router, Stack, Scene, Drawer } from 'react-native-router-flux';

import HomePage from './src/pages/HomePage';
import Login from './src/pages/Login';
import SignUp from './src/pages/Signup';
import SideBar from './src/components/SideBar';
import { Icon } from 'react-native-elements';
import ListOnePage from './src/pages/ListOnePage';
import ItemDetails from './src/components/ItemDetails';
import Color from "./src/common/Color"


export default class Routes extends Component<{}>{
   render() {
       return (

        <Router navigationBarStyle={{backgroundColor:Color.primary}}>
               <Drawer
                       key="drawer"
                       hideNavBar={true}
                       contentComponent={SideBar}
                       drawerWidth={200}
                       drawerPosition="left"
                       titleStyle={{color:'white'}}
                       styles={drawerStyles}
                       rightButtonImage={require('./src/images/carts.png')}
                       onRight={() => alert("asjfasdjb")}
                       drawerImage={require('./src/images/menus.png')} >
              
               <Stack key='root'>
                   <Scene key='login' component={Login} title='Login' initial={true} hideNavBar={true} drawer={false}/>
                   <Scene key='signup' component={SignUp} title='Signup' drawer={false} hideNavBar={true}/>
                                       
                        <Scene key='homepage' component={HomePage} title='SportsShop'/>

                 <Scene key='listonepage' component={ListOnePage} hideNavBar/>
                 <Scene key='itemdetails' component={ItemDetails} drawer={true} />

               </Stack>

               </Drawer>

           </Router>
       )
   }
}
const drawerStyles = {
    drawer: {
        shadowColor: '#000', shadowOpacity: 0.3, shadowRadius: 1,color:'black'
    }
};
       