import React, { Component } from 'react';
import { Image, } from 'react-native';
import { Container, Header, View, DeckSwiper, Card, CardItem, Thumbnail, Text, Left, Body, Icon } from 'native-base';
import Entries from '../data/Entries';


export default class Swiper extends Component {
    render() {
        
        return (
            <View style={{ height: 250 ,borderRadius:1000,marginHorizontal:20,marginVertical:5}}>
               
                <DeckSwiper
                        style={{flex:1}}
                        dataSource={Entries}
                        renderItem={item =>
                            <Card style={{ elevation: 2, borderRadius:10,backgroundColor:'blue',height:200 }}>
                                <CardItem cardBody  style={{alignSelf:'center',height:200,backgroundColor:"green"}}>
                                    <Image style={{width:null,flex:1,height:200 }} source={item.image}   />
                                </CardItem>
                               

                            </Card>
                        }
                    />
               
            </View>
        );
    }
}