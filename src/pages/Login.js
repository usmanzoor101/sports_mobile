import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ImageBackground,
    Image,
    TextInput,
    Dimensions,
    TouchableOpacity,
    StatusBar,
    
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ProgButton from '../components/ProgButton';
import SplashScreen from 'react-native-splash-screen';
import  Color from "../common/Color";
import axios from 'axios';
import {AsyncStorage} from 'react-native';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

export default class Login extends Component<{}> {






    // componentDidMount() {
    //     // do stuff while splash screen is shown
    //     // After having done stuff (such as async tasks) hide the splash screen
    //     SplashScreen.hide();

    // }

    constructor(props) {
        super(props);
        this.state = { email: "", password: "", USER_TOKEN: "", userInfo: '', };
    }

    async getData() {
        // alert("pressed");
        const res = await axios
            .post("http://192.168.8.24:8000/api/login",
                { email: 'la@g.com', password: '123456' });
        const data = await res.data.token.token;
        this.setState({ USER_TOKEN: data });
        Actions.homepage({ SendToken: this.state.USER_TOKEN });
    }

     login() {
         this.getData();
        // Actions.homepage({check:"abcd"});

    
     }

    register() {
        Actions.signup();
    }


    render() {
        
        console.log(this.state.USER_TOKEN);
        return (
            
            <View style={{ flex: 1,backgroundColor:Color.fourth}} >

                {/* <View 
                style={{ backgroundColor: 'red', overflow: 'hidden',
                 borderRadius: 300, width: 600, height: 600,
                  position: 'absolute', top: -100, alignSelf: 'center' }}>
                    <Image 
                    source={require('../images/mainhalf.jpg')} 
                    style={{ width: 600, height:600 }} blurRadius={0}>
                    </Image>
                </View> */}

                <StatusBar hidden={true} />

                <View style={styles.container} >

                    <KeyboardAwareScrollView enableAutomaticScroll={true}
                        enableOnAndroid={true}
                        extraHeight={height / 2.7}
                        enableAutomaticScroll={true}
                    >
                        <Image style={{ height: 120, width: 120, marginVertical: 20, alignSelf: 'center' }}
                            source={require('../images/logoo.png')}
                        />

                        {/* Texts */}
                        <Text style={styles.text1}>Sports-Clothing</Text>

                        <View style={{ alignItems: 'center', justifyContent: 'center', marginVertical: 10 }} >


                            <TextInput placeholder='Email' style={styles.inputText}
                                onChangeText={email => this.setState({ email })}
                                // returnKeyType='go'
                                placeholderTextColor='black'
                                ref={(input) => this.email = input}
                                onSubmitEditing={() => this.passwordInput.focus()}></TextInput>



                            <TextInput placeholder='Password' style={styles.inputText}
                                onChangeText={password => this.setState({ password })}
                                secureTextEntry={true}
                                // returnKeyType='go'
                                placeholderTextColor='black'
                                ref={(input) => this.passwordInput = input}></TextInput>



                         <TouchableOpacity style={styles.loginButton}
                             onPress={this.login.bind(this)} >
                                                      
                             <Text style={styles.text}>LOGIN</Text>
                         </TouchableOpacity>

                             
                             {/* <View style={styles.buttonView}>
                                 <ProgButton 
                                onClick={this.login.bind(this)}/>
                            </View> */}


                        </View>


                    </KeyboardAwareScrollView>


                    <View style={{ bottom: 0, position: 'absolute', justifyContent: 'center', alignItems: 'center', }}>
                        <Text style={styles.text4}>Do not have an account? Register here</Text>
                        <TouchableOpacity style={styles.registerButton}
                            onPress={this.register.bind(this)} >
                            <Image style={styles.imageReg} source={require('../images/reg.png')} ></Image>
                            <Text style={styles.text}>REGISTER</Text>
                        </TouchableOpacity>
                    </View>
                </View>


            </View>



        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        
    },
    imgBackground: {
        width: width,
        height: height,
    },
    text1: {
        alignSelf: 'center',
        fontSize: 26,
        fontWeight: 'bold',
        color: Color.secondary,
        fontStyle: 'italic'
    },
    text4: {
        alignSelf: 'center',
        fontSize: 17,
        color: Color.secondary,
        marginTop: 6,
    },
    inputText: {
        //backgroundColor:'transparent',
        backgroundColor: 'rgba(255,255, 255, 0.3)',
        width: 320,
        fontSize: 20,
        marginTop: 10,
        paddingLeft: 10,
        height: 50,
        color: 'white',
        borderWidth: 2,
        borderRadius: 5,
        borderColor: '#D0D0D0'
    },

    image: {
        width: 35,
        height: 35,
        marginTop: 25,
        marginRight: 10,
    },
    inputRow: {
        flexDirection: 'row',
        alignSelf: 'center',
    },
    loginButton: {
        borderColor: 'white', borderWidth: 1,
        width: width/2, borderRadius: 5,
        height:60,
        marginTop:50,
        alignSelf:'center',
        justifyContent:'center',
        backgroundColor: Color.secondary,
    },

    registerButton: {
        borderColor: 'white', borderWidth: 1,
        
        width: width, borderRadius: 5,
        backgroundColor: Color.secondary,
    },

    text: {
        color: 'white',
        fontSize: 20,
        alignSelf: 'center',
        fontWeight:'bold'
        
    },
    imageReg: {
        width: 23,
        height: 23,
        alignSelf: 'center',
    },

});