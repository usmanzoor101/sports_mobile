import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    ImageBackground,
    Image,
    TextInput,
    Dimensions,
    KeyboardAvoidingView,
    TouchableOpacity,
    StatusBar

} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
var width = Dimensions.get('screen').width; //full width
var height = Dimensions.get('screen').height; //full height
import { Actions } from 'react-native-router-flux';
import axios from 'axios';
export default class Signup extends Component {

    mainPage() {
        Actions.login();
    }


    constructor(props) {
        super(props);
        this.state = { email: "", password: "", USER_TOKEN: "", name: "" };
    }

    // async getData() {
    //     const res = await axios
    //         .post("http://192.168.10.16:8000/api/register",
    //             { email: this.state.email, password: this.state.password, name: this.state.name });
    //     const data = await res.data.success.token;
    //     this.setState({ USER_TOKEN: data });
    //     Actions.mainmenu({ SendToken: this.state.USER_TOKEN });
    // }

    logged() {
       // this.getData();
       Actions.login();
    }
    render() {
        console.log(this.state);
        return (

            <ImageBackground style={styles.imgBackground}
                resizeMode='cover'
                blurRadius={0.2}
                source={require('../images/mainhalf.jpg')}>

                <View style={{ flex: 1, justifyContent: 'center' }} >
                    <View>
                        <KeyboardAwareScrollView enableAutomaticScroll={true}
                            enableOnAndroid={true}
                            extraHeight={height / 2.7}
                            enableAutomaticScroll={true}
                        >
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <TextInput placeholder='Name' style={styles.inputText}
                                    onChangeText={name => this.setState({ name })}
                                    // returnKeyType='go'
                                    placeholderTextColor='white'
                                    ref={(input) => this.email = input}
                                    onSubmitEditing={() => this.emailInput.focus()}></TextInput>

                                <TextInput placeholder='Email' style={styles.inputText}
                                    onChangeText={email => this.setState({ email })}
                                    // returnKeyType='go'
                                    placeholderTextColor='white'
                                    ref={(input) => this.emailInput = input}
                                    onSubmitEditing={() => this.passwordInput.focus()}></TextInput>


                                <TextInput placeholder='Password' style={styles.inputText}
                                    onChangeText={password => this.setState({ password })}
                                    secureTextEntry={true}
                                    // returnKeyType='go'
                                    placeholderTextColor='white'
                                    ref={(input) => this.passwordInput = input}
                                    onSubmitEditing={() => this.passwordConfirmInput.focus()}></TextInput>


                                <TextInput placeholder='Confirm Password' style={styles.inputText}
                                    onChangeText={Confirm_Password => this.setState({ Confirm_Password })}
                                    secureTextEntry={true}
                                    // returnKeyType='go'
                                    placeholderTextColor='white'
                                    ref={(input) => this.passwordConfirmInput = input}
                                ></TextInput>

                                <TouchableOpacity onPress={this.logged.bind(this)} style={styles.buttonView}>
                                    <Image style={styles.imageReg} source={require('../images/reg.png')} ></Image>
                                    <Text style={styles.textlog}  >LOGIN</Text>
                                </TouchableOpacity>

                            </View>
                        </KeyboardAwareScrollView>


                    </View>

                    <View style={{ bottom: 55, position: 'absolute', justifyContent: 'center', alignItems: 'center', }}>
                        <Text style={styles.text4}>Already have an account? Login here</Text>
                        <TouchableOpacity style={styles.buttonView2}
                            onPress={this.mainPage.bind(this)} >
                            <Image style={styles.imagelog} source={require('../images/login.png')} ></Image>
                            <Text style={styles.textlog}>Log In</Text>
                        </TouchableOpacity>
                    </View>

                </View>

            </ImageBackground>



        );
    }
}
const styles = StyleSheet.create({

    buttonView: {
        flexDirection: 'row',
        borderColor: '#003366',
        width: 320,

        backgroundColor: 'rgba(214, 69, 65, 1)', paddingVertical: 10, marginVertical: 5,

    },
    text4: {
        alignSelf: 'center',
        fontSize: 17,
        color: 'white',
        marginTop: 6,
    },
    buttonView2: {
        borderColor: 'white', borderWidth: 1,
        width: width, borderRadius: 5,
        backgroundColor: 'rgba(214, 69, 65, 1)',


    },
    imgBackground: {
        width: width,
        height: height,
    },
    textlog: {
        color: 'white',
        fontSize: 20,
        alignSelf: 'center',
    },
    imageReg: {
        width: 30,
        height: 28,
        marginTop: 2,
        marginLeft: 110,
    },
    imagelog: {
        width: 23,
        height: 23,
        alignSelf: 'center',
    },

    inputText: {
        //backgroundColor:'transparent',
        backgroundColor: 'rgba(255,255, 255, 0.3)',
        width: 320,
        fontSize: 20,
        marginTop: 10,
        paddingLeft: 10,
        height: 50,
        color: 'black',
        borderWidth: 2,
        borderRadius: 5,
        borderColor: '#D0D0D0'
    },

    text: {
        color: 'white',
        fontSize: 20,
        alignSelf: 'center',
    },
    inputRow: {
        alignSelf: 'center',
        margin: 5
    },

});