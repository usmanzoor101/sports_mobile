import React, { Component } from 'react';
import Slideshow from 'react-native-image-slider-show';
import MyData from '../data/MyData';



export default class MySlider extends Component {
    constructor(props) {
        super(props);

        this.state = {
            position: 1,
            interval: null,
            dataSource: MyData
        };
    }

    componentWillMount() {
        this.setState({
            interval: setInterval(() => {
                this.setState({
                    position: this.state.position === this.state.dataSource.length ? 0 : this.state.position + 1
                });
            }, 1500)
        });
    }

    componentWillUnmount() {
        clearInterval(this.state.interval);
    }

    render() {
        return (
            <Slideshow
                dataSource={this.state.dataSource}
                position={this.state.position}
                onPositionChanged={position => this.setState({ position })}
                indicatorSize={10}
                
                arrowSize={0}
                //containerStyle={{marginHorizontal:30}} 
                />
        );
    }
}