import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    Image
} from 'react-native';


import { Content } from 'native-base';
import { Divider } from 'react-native-elements';
import Color from "../common/Color"

export default class SideBar extends Component {
    render() {
        return (
            <Content style={{
                backgroundColor: Color.secondary }}
                >

                <Image style={styles.logo} source={require('../images/logoo.png')} ></Image>

                <Text style={styles.texts}>HOME</Text>
                <Divider />
                <Text style={styles.texts}>MAN</Text>
                <Divider />
                <Text style={styles.texts}>WOMAN</Text>
                <Divider />
                <Text style={styles.texts}>PAGES</Text>
                <Divider />
                <Text style={styles.texts}>ABOUT</Text>
                <Divider />
                
            </Content>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    texts: {
        fontSize:15,
        marginVertical:15,
        color:Color.third,
        fontWeight:'bold',
        marginLeft:30
        
    },

    logo:{
        marginTop:20,
        alignSelf:'center',
        width:70,
        height:80,

    }
});