import React, { Component } from 'react';
import {
    Text,
    View,
    ScrollView,
    DrawerLayoutAndroid,
    SectionList,
    ListRenderItem,
    StyleSheet,
    Dimensions,
    FlatList,
 
    Image
} from 'react-native';
import {Container, Content} from 'native-base';
import { Col, Row, Grid } from "react-native-easy-grid";

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height



export default class ListOnePage extends Component<{}> {

  render() {
    return (
      <View style={{flex:1}}>
               <View 
                style={{ backgroundColor: 'red', overflow: 'hidden',
                  height: 150,
                   }}>
    
                    <Image 
                    source={require('../images/mainhalf.jpg')} 
                    style={{ width: width, height:150 }} blurRadius={0}>
                    </Image>
                    <View style={{ position: 'absolute', top: 0, left: 0, right: 0, height: 150, alignItems: 'center', justifyContent: 'center' }}>
               <Text style={{ color: '#FFF', fontSize: 23 }}>{this.props.text}</Text>
               

               
                    </View>
              </View> 
                  <Container>
                  <Content>
                      <Grid >
                          <Col style={{ backgroundColor: '#D954D7', height: 70,width:60 }}></Col>
                          <Col style={{ backgroundColor: '#D954D7', height: 70,width:60  }}></Col>
                          
                          <Row style={{backgroundColor:'black',height:70,width:60}} ></Row>
                      </Grid>
                  </Content>
              </Container>
              </View>

      // <View
      //   // backgroundImage={require('../images/mainhalf.jpg')}
      //   headerChildren={
      //     <View style={styles.foregroundContainer}>
      //       <Image
      //         source={require('../images/mainhalf.jpg')}
      //         style={{height:100}}
      //         resizeMode="contain"
      //       />
      //       <View style={{ backgroundColor: 'transparent' }}>
      //         <Text style={{ color: '#FFF', fontSize: 23 }}>Donald Duck</Text>
      //         <Text style={{ color: '#FFF', fontSize: 20 }}>d.duck</Text>
      //       </View>
      //       <View >
      //         <TouchableOpacity>
      //           <Text style={{ color: '#FFF', fontSize: 17 }}>Logout</Text>
      //         </TouchableOpacity>
      //       </View>
      //     </View>
    //     }
    //   >
    //     <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', height: 600 }}>
    //       <Text>Content</Text>
    //     </View>
    //   </ImageHeader>
    // )
    )};
}

  const styles = StyleSheet.create({
  userImage: {
    width: 50,
    height: 50,
    marginHorizontal: 15
  },
  foregroundContainer: {
    width: Dimensions.get('window').width,
    height: 140,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  logoutButton: {
    position: 'absolute',
    top: 60,
    right: 15
  }
});