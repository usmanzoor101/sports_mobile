import React, { Component } from 'react';
import { Container, Header, Content, Footer, FooterTab, Button, Icon, Text } from 'native-base';
import {
    StyleSheet,
    Image,
    TextInput,
    Dimensions,
    TouchableOpacity,
    View,

} from 'react-native';
import { Divider } from 'react-native-elements';
import Color from '../common/Color';
export default class MyFooter extends Component {

    render() {
        return (
            <View style={{alignItems:'center',alignContent:'center',marginBottom:3,borderTopWidth:1}} >
                <View style={{marginVertical:30}}>
                    <View >
                    <View style={{marginBottom:15}} >
                    <Text style={styles.Text1}>All Genuine Products</Text> 
                    <Text style={styles.Text2}>Copyrights @ 2019</Text> 
                    </View>
                    <Divider/>

                    <View style={{marginBottom:15}} >
                    <Text style={styles.Text1}>400+ Products</Text> 
                    <Text style={styles.Text2}>Find All You Need</Text> 
                    </View>
                    <Divider/>

                    <View style={{marginBottom:15}} >
                    <Text style={styles.Text1}>Money Back Guarantee</Text> 
                    <Text style={styles.Text2}>Customer Satisfaction</Text> 
                    </View>
                    
                    </View>
            </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    Text1:{
        fontSize: 22, margin: 2,fontWeight:'bold',
        color:Color.primary, alignSelf:'center'
    },
    Text2:{
        fontSize: 15, margin: 2,alignSelf:'center',fontStyle:'italic'
    }

});