import React, { Component } from 'react';
import {
    Text,
    View,
    ScrollView,
    DrawerLayoutAndroid,
    SectionList,
    ListRenderItem,
    StyleSheet,
    Dimensions,
    FlatList,
    TouchableOpacity,
    Image
  

} from 'react-native';

import { Actions } from 'react-native-router-flux';
import axios from 'axios';
import { Drawer,Button } from 'native-base';
import MySlider from '../components/MySlider';
import Categories from '../components/Categories';
import { animateElastic } from 'react-native-really-awesome-button/src/helpers';
import Swiper from '../components/Swiper';
import MyFooter from '../components/MyFooter';
import ListOne from '../data/ListOne';
import RenderListOne from '../components/RenderListOne';
import MySearchBar from "../components/MySearchBar";
import MySpinner  from '../components/MySpinner';

export default class HomePage extends Component {

    state={Products:[],loading: true};

    componentWillMount()
    {
      this.getData();
      
    }
    async getData() {
        const TOKEN =  this.props.SendToken;
        const AuthStr = "Bearer ".concat(TOKEN);
        const res = await axios
            .get("http://192.168.8.24:8000/api/showProducts",
                { headers:{Authorization:AuthStr}});
        const dataget = await res.data.products;
        this.setState({ Products: dataget, loading:false });
        
       
    }

    renderShow(){
        if (this.state.loading){
            return(<MySpinner/>)
        }
        if (!this.state.loading){
            return( 
            <FlatList
                horizontal={true}
                data={this.state.Products}
                contentContainerStyle={{ alignItems: 'center', justifyContent: 'center' }}
                renderItem={this.renderItems}
                keyExtractor={(item, index) => index.toString()}
            />
            )
        }
    }


    goTo(head){
        Actions.listonepage({text:head})
    };

    closeDrawer = () => {
        this.drawer._root.close()
    };
    openDrawer = () => {
        animateElastic
        this.drawer._root.open()
    };
    renderItems = ({ item }) => {
        return (
            <RenderListOne
            //  author={item.author} 
            //   book_title={item.book_title} 
             image={item.image} 
            product_name={item.product_name}
            slug={item.slug}
             />
        );
    }
   
    render() {
        console.log(this.state.Products)
        return (
            
          
            <ScrollView style={{flex:1}}>
  <MySearchBar/>
{/* Image slider Component */}
                <MySlider />
{/* Search bar and categories slider Component */}                  
                <Categories/>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
                    <Text style={styles.MainTexts}>Sale Items</Text>
            
                   

                    <TouchableOpacity onPress={()=>{this.goTo('Sale Items')}} style={styles.ShowAll} ><Text>Show All >></Text></TouchableOpacity>


                </View>

               
                {this.renderShow()}


                 
                 <View style={{flexDirection:'row',justifyContent:'space-between'}} >
                <Text style={styles.MainTexts}>Featured Items</Text>

                    <TouchableOpacity onPress={()=>{this.goTo('Featured Items')}} style={styles.ShowAll}><Text>Show All >></Text></TouchableOpacity>                   
                     
               
                </View> 
             
              <FlatList
                    horizontal={true}
                    data={this.state.Products}
                    contentContainerStyle={{alignItems:'center',justifyContent:'center'}}
                    renderItem={this.renderItems}
                    keyExtractor={(item, index) => index.toString()}
                />

                {/* <Swiper /> */}
                <View style={{ flexDirection: 'row', justifyContent: 'space-between'}}>
                <Text style={styles.MainTexts}>Recently Viewed Items</Text>
                    
                    <TouchableOpacity onPress={()=>{this.goTo('Recently Viewed Items')}} style={styles.ShowAll} >
                    <Text>Show All >>

                    </Text>
                    </TouchableOpacity>

                    
                </View>
                <FlatList
                    horizontal={true}
                    data={this.state.Products}
                    contentContainerStyle={{ alignItems: 'center', justifyContent: 'center' }}
                    renderItem={this.renderItems}
                    keyExtractor={(item, index) => index.toString()}
                />

                {/* <View style={{flexDirection:'row',backgroundColor:'red'}} >
                   <Image 
                        source={require('../images/money.gif')}
                        style={{width: 100, height: 100 }}
                    >
                    </Image>

                    <Image
                        source={require('./../images//support.gif')}
                        style={{ width: 100, height: 100 }}
                    />


                </View> */}

                <MyFooter/>

                 
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginVertical: 20,
    },
    ShowAll:{
        justifyContent:'flex-end',
        fontSize: 17,
        marginRight:10,
        marginBottom:5
        
    },
    MainTexts:{
        fontWeight: 'bold', marginVertical: 15, fontSize: 22,marginHorizontal:10
    }
    
});
           
