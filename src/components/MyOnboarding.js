import React, { Component, ViewPagerAndroid, View, Text } from 'react-native';

export default class Onboarding extends Component {

    render() {

        return (
          <ViewPagerAndroid style={{ flex: 1 }} initialPage={0}>
            <View style={{alignItems: 'center', padding: 20}}>
              <Text>First page</Text>
            </View>
            <View style={{alignItems: 'center', padding: 20}}>
              <Text>Second page</Text>
            </View>
          </ViewPagerAndroid>
        );

    }

}