import React, { Component } from "react";
import {
    StyleSheet,
    Text,
    Image,
    View,
    TouchableOpacity
} from "react-native";
import { Actions } from 'react-native-router-flux';

const RenderListOne = (props) => {
    _onPress = () => {
    Actions.itemdetails();        
        
    };
    console.log(this.props);
    return (
        <View>
            {/* //postman name */}
            <Text style={{alignSelf:'center'}} >{props.product_name}</Text>
            <Text>{props.slug}</Text>
            
            
            
            {/* <Text>{props.book_title}</Text>
            <Text>{props.author}</Text> */}
              <TouchableOpacity onPress={this._onPress}>
            <View style={styles.imageHolderView}>
            <Image style={{ width: 80, height: 150 }}  source={{ uri: props.image }} />   
         </View>
            </TouchableOpacity>

        </View>
    )
}

const styles = StyleSheet.create({
    imageHolderView: {
        borderRadius: 10,
        borderWidth: 2,
        elevation: 2,
        borderColor: 'gray',
        marginHorizontal: 5,
        overflow: 'hidden',
        height: 150,
    },

});

export default RenderListOne;